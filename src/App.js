import React, {useState, useEffect} from 'react';
import {View, ScrollView} from 'react-native';
import SampleComponent from './pages/SampleComponent';
import StylingItem from './pages/StylingItem';
import StylingComponent from './pages/StylingComponent';
import FlexBox from './pages/FlexBox';
import PositionReactNative from './pages/PositionReactNative';
import PropsDinamis from './pages/PropsDinamis';
import StateDinamis from './pages/StateDinamis';
import Communications from './pages/Communications';
import ReactNativeSVG from './pages/ReactNativeSVG';
import BasicJavascript from './pages/BasicJavascript';

const App = () => {
  //uncommand if want touse componnet will unmount
  const [isShow, SetIsShow] = useState(true);
  // useEffect(() => {
  //   setTimeout(() => {
  //     SetIsShow(false);
  //   }, 6000);
  // }, []);
  return (
    <View>
      <ScrollView>
        <SampleComponent />
        <StylingComponent />
        <StylingItem />
        {/* {isShow && <FlexBox />} */}
        <FlexBox />
        <PositionReactNative />
        <PropsDinamis />
        <StateDinamis />
        <Communications />
        <ReactNativeSVG />
        <BasicJavascript />
      </ScrollView>
    </View>
  );
};

export default App;
