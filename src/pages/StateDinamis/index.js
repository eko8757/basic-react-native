import React, {useState, Component} from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'

const StateDinamis = () => {
    return (
        <View style={styles.wrapper}>
            <Text style={styles.txtTitle}>Materi State Dinamis</Text>
            <Text style={styles.titleSection}>Fucntion Component State</Text>
            <Counter />
            <Counter />
            <Text style={styles.titleSection}>Class Component State</Text>
            <CounterClass />
            <CounterClass />
        </View>
    );
};

const Counter = () => {
    const [number, setNumber] = useState(0);
    return (
        <View>
            <Text>{number}</Text>
            <Button title="Tambah" onPress={() => setNumber(number + 1)} />
        </View>
    );
};

class CounterClass extends Component {
    state = {
        number: 0,
    };
    render() {
        return (
            <View>
                <Text>{this.state.number}</Text>
                <Button title="Tambah" onPress={() => this.setState({number: this.state.number + 1})} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        padding: 20,
    },
    txtTitle: {
        textAlign: 'center',
    },
    titleSection: {
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 12,
    },
});

export default StateDinamis;
