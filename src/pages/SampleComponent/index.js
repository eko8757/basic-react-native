import React, {Component} from 'react';
import {View, Text, TextInput, Image, StyleSheet} from 'react-native';

const SampleComponent = () => {
  return (
    <View>
      <View style={styles.parrent} />
      <Text>Hello</Text>
      <Dot />
      <Text>World</Text>
      <Gambar />
      <TextInput style={styles.txtInput} />
      <BoxGreen />
      <Profile />
    </View>
  );
};

const Dot = () => {
  return <Text>...</Text>;
};

const Gambar = () => {
  return (
    <Image
      source={{uri: 'http://placeimg.com/100/100/any'}}
      style={styles.img}
    />
  );
};

//class component
class BoxGreen extends Component {
  render() {
    return <Text>Ini component dari class</Text>;
  }
}

class Profile extends Component {
  render() {
    return (
      <View>
        <Image
          source={{uri: 'http://placeimg.com/100/100/any'}}
          style={styles.imgProfile}
        />
        <Text>Ini text dari image class component</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  parrent: {
    height: 80,
    backgroundColor: '#0abde3',
  },
  txtInput: {
    marginStart: 16,
    marginEnd: 16,
    marginTop: 16,
    borderWidth: 1,
  },
  img: {
    width: 100,
    height: 100,
  },
  imgProfile: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
});

export default SampleComponent;
