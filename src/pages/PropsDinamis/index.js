import React from 'react'
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native'

const PropsDinamis = () => {
    return (
        <View>
            <Text>Materi Props Dinamis</Text>
            <ScrollView horizontal>
                <View style={styles.wrapper}>
                    <Story
                        judul="Drumming"
                        gambar="https://scontent-sin6-1.cdninstagram.com/v/t51.12442-15/e35/c0.150.480.480a/s150x150/57402399_404263726796071_2860650612150800450_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=107&_nc_ohc=gfBdFBSOiIYAX9ncf79&oh=597b61b789f9c2754cf896aaf115dd55&oe=5F3DF26C" />
                    <Story
                        judul="Programming"
                        gambar="https://scontent-sin6-1.cdninstagram.com/v/t51.2885-15/s150x150/73423629_1215195292005667_7742609274562926689_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=107&_nc_ohc=8vbv58T58nYAX-85JCV&oh=1ad917815ce2491597f4bca1a340ef17&oe=5F637A07" />
                    <Story
                        judul="Children"
                        gambar="https://scontent-sin6-1.cdninstagram.com/v/t51.2885-15/s150x150/76800037_479425606017871_2639504434885924830_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=107&_nc_ohc=42EDf9Db9lMAX-Yq4Qv&oh=4c2c126bda9bb3cfe0b54860d986b9f6&oe=5F642E20" />
                    <Story
                        judul="Motivation"
                        gambar="https://scontent-sin6-1.cdninstagram.com/v/t51.2885-15/s150x150/75426222_457529918279694_1425943910819402443_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=106&_nc_ohc=nlgmvr1IjB0AX8ehXCL&oh=defd710092b50b998f28e2d7d96b7e0d&oe=5F63DE02" />
                    <Story
                        judul="Motivation"
                        gambar="https://scontent-sin6-1.cdninstagram.com/v/t51.2885-15/s150x150/75426222_457529918279694_1425943910819402443_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=106&_nc_ohc=nlgmvr1IjB0AX8ehXCL&oh=defd710092b50b998f28e2d7d96b7e0d&oe=5F63DE02" />
                    <Story
                        judul="Motivation"
                        gambar="https://scontent-sin6-1.cdninstagram.com/v/t51.2885-15/s150x150/75426222_457529918279694_1425943910819402443_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=106&_nc_ohc=nlgmvr1IjB0AX8ehXCL&oh=defd710092b50b998f28e2d7d96b7e0d&oe=5F63DE02" />
                </View>
            </ScrollView>
        </View>
    )
}

const Story = (props) => {
    return (
        <View style={styles.wrapperContent}>
            <Image source={{
                uri: props.gambar}}
                style={styles.imgStory}
            />
            <Text style={styles.maxWidth}>{props.judul}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    imgStory: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    wrapper: {
        flexDirection: 'row',
        marginTop: 16,
    },
    txtStory: {
        maxWidth: 50,
        textAlign: 'center',
    },
    wrapperContent: {
        alignItems: 'center',
        marginStart: 20,
    },
});

export default PropsDinamis;
