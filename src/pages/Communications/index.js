import React, {useState} from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Cart from '../../components/Cart';
import Product from '../../components/Product';

const Communications = () => {
    const [totalProduct, setTotalProduct] = useState(0);
    return (
        <View style={styles.container}>
            <Text style={styles.txtTitle}>Materi Komunikasi Antar Komponen</Text>
            <Product onButtonPress={() => setTotalProduct(totalProduct + 1)}/>
            <Cart quantity={totalProduct}/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    txtTitle: {
        textAlign: 'center',
    },
});

export default Communications;
