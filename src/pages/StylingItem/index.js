import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import imageItem from '../../assets/image/home.jpg';

const StylingItem = () => {
  return (
    <View style={styles.parent}>
      <Image source={imageItem} style={styles.imgItem} />
      <Text style={styles.textItem}>My Desk Setup</Text>
      <Text style={styles.textCurrency}>Rp. 999.999.999</Text>
      <Text style={styles.textAddress}>Kota Malang</Text>
      <View style={styles.bgButton}>
        <Text style={styles.textBtn}>BELI</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
    parent : {
    marginTop: 16,
    marginStart: 16,
    width: 212,
    padding: 12,
    borderRadius: 8,
    backgroundColor: '#F2F2F2',
  },
  imgItem: {
    width: 188,
    height: 107,
    borderRadius: 8,
  },
  textItem: {
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 12,
  },
  textCurrency: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#F2994A',
    marginTop: 8,
  },
  textAddress: {
    fontSize: 14,
    fontWeight: 'normal',
    marginTop: 4,
  },
  textBtn: {
    fontSize: 18,
    fontWeight: '600',
    color: '#FFFFFF',
    textAlign: 'center',
  },
  bgButton: {
    backgroundColor: '#10ac84',
    borderRadius: 25,
    marginTop: 16,
    paddingVertical: 6,
  },
});

export default StylingItem;
