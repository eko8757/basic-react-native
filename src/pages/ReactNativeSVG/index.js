import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Ilustrations from '../../assets/image/ilustration.svg';

const ReactNativeSVG = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.txtTitle}>Materi React Native SVG</Text>
            <Ilustrations width={244} height={125} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    txtTitle: {
        textAlign: 'center',
    }
});

export default ReactNativeSVG;
