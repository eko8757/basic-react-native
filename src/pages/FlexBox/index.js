import React, {Component, useEffect, useState} from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';

class FlexBox extends Component {

    //uncommand if want to test class component lifecycle
    // constructor(props) {
    //     super(props);
    //     console.log('==> constructor');
    //     this.state = {
    //         subscriber: 25,
    //     };
    // }

    // componentDidMount() {
    //     console.log('==> Component didmount');
    //     setTimeout(() => {
    //         this.setState({
    //             subscriber: 400,
    //     });
    //     }, 2000);
    // }

    // componentDidUpdate() {
    //     console.log('==> Component DidUpdate');
    // }

    // componentWillUnmount() {
    //     console.log('==> Component WillUnmount');
    // }

    render() {
        // console.log('==> render');
        return (
            <View>
                <View style={{
                    flexDirection: 'row',
                    backgroundColor: '#c8d6e5',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}>
                    <View style={{width: 50, height: 50, backgroundColor: '#ee5253'}} />
                    <View style={{width: 50, height: 50, backgroundColor: '#feca57'}} />
                    <View style={{width: 50, height: 50, backgroundColor: '#1dd1a1'}} />
                    <View style={{width: 50, height: 50, backgroundColor: '#5f27cd'}} />
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-around',}}>
                    <Text>Beranda</Text>
                    <Text>Video</Text>
                    <Text>Playlist</Text>
                    <Text>Komunitas</Text>
                    <Text>Channel</Text>
                    <Text>Tentang</Text>
                </View>
                <View style={styles.parrentStyle}>
                    <Image
                        source={{
                            uri: 'https://yt3.ggpht.com/a-/AOh14GiYsEjg9XX-d82Jw_NYhNqjUAlHb03CIRaP7kbvrw=s100-c-k-c0xffffffff-no-rj-mo'}}
                            style={styles.imgStyle}
                    />
                    <View style={styles.contentStyle}>
                        <Text style={styles.txtStyle}>Eko Murdiansyah</Text>
                        <Text style={styles.txtStyle2}>20 Jt Subscriber</Text>
                        {/* <Text style={styles.txtStyle2}>{this.state.subscriber} Jt Subscriber</Text> */}
                    </View>
                </View>
            </View>
        );
    }
}

// const FlexBox = () => {

//     //if want to see lifecycle function component
//     // const [subscriber, setSubscriber] = useState(200);
//     // useEffect(() => {
//     //     console.log('did mount');
//     //     setTimeout(() => {
//     //         setSubscriber(400);
//     //     }, 2000);
//     //     return () => {
//     //         console.log('did update');
//     //     };
//     // }, [subscriber]);

//     // useEffect(() => {
//     //     console.log('did update');
//     //     setTimeout(() => {
//     //         setSubscriber(400);
//     //     }, 2000);
//     // }, [subscriber]);
//     return (
//         <View>
//                 <View style={{
//                     flexDirection: 'row',
//                     backgroundColor: '#c8d6e5',
//                     alignItems: 'center',
//                     justifyContent: 'space-between',
//                 }}>
//                     <View style={{width: 50, height: 50, backgroundColor: '#ee5253'}} />
//                     <View style={{width: 50, height: 50, backgroundColor: '#feca57'}} />
//                     <View style={{width: 50, height: 50, backgroundColor: '#1dd1a1'}} />
//                     <View style={{width: 50, height: 50, backgroundColor: '#5f27cd'}} />
//                 </View>
//                 <View style={{flexDirection: 'row', justifyContent: 'space-around',}}>
//                     <Text>Beranda</Text>
//                     <Text>Video</Text>
//                     <Text>Playlist</Text>
//                     <Text>Komunitas</Text>
//                     <Text>Channel</Text>
//                     <Text>Tentang</Text>
//                 </View>
//                 <View style={styles.parrentStyle}>
//                     <Image
//                         source={{
//                             uri: 'https://yt3.ggpht.com/a-/AOh14GiYsEjg9XX-d82Jw_NYhNqjUAlHb03CIRaP7kbvrw=s100-c-k-c0xffffffff-no-rj-mo'}}
//                             style={styles.imgStyle}
//                     />
//                     <View style={styles.contentStyle}>
//                         <Text style={styles.txtStyle}>Eko Murdiansyah</Text>
//                         <Text style={styles.txtStyle2}>{subscriber} Jt Subscriber</Text>
//                         {/* <Text style={styles.txtStyle2}>20 Jt Subscriber</Text> */}
//                         {/* <Text style={styles.txtStyle2}>{this.state.subscriber} Jt Subscriber</Text> */}
//                     </View>
//                 </View>
//             </View>
//     );
// }

const styles = StyleSheet.create({
    imgStyle: {
        width: 100,
        height: 100,
        borderRadius: 50,
    },
    parrentStyle: {
        flexDirection: 'row',
        marginTop: 16,
        marginStart: 16,
        alignItems: 'center',
    },
    contentStyle: {
        marginStart: 8,
    },
    txtStyle: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    txtStyle2: {
        fontSize: 14,
        fontWeight: 'bold',
    },
});

export default FlexBox;
