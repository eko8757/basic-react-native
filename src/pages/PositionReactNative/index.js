import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import cartShopping from '../../assets/icon/supermarket.png';

const PositionReactNative = () => {
    return (
        <View style={styles.wrapper}>
            <Text>Materi Position</Text>
            <View style={styles.cartWrapper}>
                <Image source={cartShopping} style={styles.icon} />
                <Text style={styles.notif}>10</Text>
            </View>
            <Text style={styles.txt}>Keranjang belanjaan anda</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    txt: {
        fontSize: 16,
        fontWeight: '700',
        marginTop: 8,
    },
    icon: {
        width: 50,
        height: 50,
    },
    cartWrapper: {
        borderWidth: 1,
        borderColor: '#4398D1',
        width: 93,
        height: 93,
        borderRadius: 93 / 2,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        marginTop: 20,
    },
    wrapper: {
        padding: 20,
        alignItems: 'center',
    },
    notif: {
        fontSize: 12,
        color: 'white',
        backgroundColor: '#10ac84',
        padding: 4,
        borderRadius: 25,
        width: 24,
        height: 24,
        position: 'absolute',
        top: 0,
        right: 0,
    },
});

export default PositionReactNative;
