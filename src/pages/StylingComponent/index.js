import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const StylingComponent = () => {
  return (
    <View>
      <Text style={styles.text}>Styling Component</Text>
      <View style={styles.box} />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#10ac84',
    marginStart: 16,
    marginTop: 16,
  },
  box: {
    width: 100,
    height: 100,
    backgroundColor: '#0abde3',
    borderWidth: 2,
    borderColor: '#5f27cd',
    marginTop: 20,
    marginStart: 16,
  },
});

export default StylingComponent;
