import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const BasicJavascript = () => {

    //tipe data
    //primitive
    const nama = 'Eko Murdiansyah';
    let usia = 23;
    const isMan = true;

    //complex
    //object
    const hewanPeliharaan = {
        nama: 'Meong',
        jenis: 'Kucing',
        usia: 2,
        isLocalAnimal: true,
        warna: 'kuning',
        orangTua: {
            jantan: 'King',
            betina: 'Queen',
        },
    };

    //function
    const sapaOrang = (name, age) => {
        return console.log(`Hello ${name} usia anda ${age}`);
    };

    sapaOrang('Eko', 23);

    //array
    const namaOrang = ['Eko', 'Murdiansyah', 'Pulungan', 'Joko'];
    typeof namaOrang; //object

    // if (hewanPeliharaan.nama == 'Meong') {
    //     console.log('Halo meong');
    // } else {
    //     console.log('Kucing siapa?');
    // }

    const sapaHewan = objectHewan => {
        // let hasilSapa = '';
        // if (objectHewan.nama === 'Meong') {
        //     hasilSapa = 'Halo meong';
        // } else {
        //     hasilSapa = 'Kucing siapa?';
        // }
        // return hasilSapa;

        //turnary operator
        return objectHewan.nama === 'Meong' ? 'Halo meong' : 'Kucing siapa?';

        //looping

    };

    return (
        <View style={styles.container}>
            <Text style={styles.txtTitle}>Materi tentang basic javascript</Text>
            <Text style={styles.txtTitle}>Nama: {nama}</Text>
            <Text>{sapaHewan(hewanPeliharaan)}</Text>
            <Text>{namaOrang[0]}</Text>
            <Text>{namaOrang[1]}</Text>
            <Text>{namaOrang[2]}</Text>
            <Text>===========</Text>
            {namaOrang.map((orang) => <Text>{orang}</Text>)}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    txtTitle: {
        textAlign: 'center',
    },
});

export default BasicJavascript;
